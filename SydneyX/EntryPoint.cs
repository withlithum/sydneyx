﻿
using Rage.Attributes;

[assembly: Plugin("SydneyX", Author = "WithLithum & contributors", Description = "Law enforcement simulation, redefined")]

namespace SydneyX
{
    /// <summary>
    /// The entry point of the plugin.
    /// </summary>
    public static class EntryPoint
    {
        /// <summary>
        /// Defines the entry point for the plugin.
        /// </summary>
        /// <remarks>
        /// This method is the entry point considered by the RPH when loading this plugin.
        /// <br />
        /// <note type="warning">
        /// Do <b>not</b> attempt to define another method with the name "Main", even if there is entry point defined in the assembly attribute.
        /// </note>
        /// </remarks>
        public static void Main()
        {

        }
    }
}